Dépot retrouvé sur mon ordi.
Il est possible qu'il contienne une version plus à jour de plotModel situé dans le dépot scanettetools

Ce dépot contient en plus deux méthodes arbitraires pour évaluer la pertinance d'un cluster sur le modèle Scanette : 
`eval_clusters_1` et `eval_clusters_2` qui prennent en argument une trace agilkia enregistrant des clusters et rendent un score basé sur un comptage des états ou des transitions.
Je n'ai plus le détail du score en tête mais c'était basé sur la similitude des transitions empruntés entre les trace d'un même cluster et la différence des transitions empruntées entre des traces de clusters différents.
